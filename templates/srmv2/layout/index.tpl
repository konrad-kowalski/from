<!DOCTYPE html>
<html lang="pl-PL">
    <head>
        <meta charset="UTF-8" />
        <meta name="format-detection" content="telephone=no" />
        {include file="./partial/html_title.tpl"}
        {include file="./partial/_favicons.tpl"}
        {assign var="jsAPIVersion" value='aukcja.javascript_api_version'|srm_config}
        {include_media path="bootstrap/bootstrap-select.css" type="css"}
        {include_media path="layout.css" type="css"}
        {include_media path="jquery-2.1.1.min.js" type="js"}
        {include_media path="jquery.scrollbar.min.js" type="js"}
        {include_media path="bootstrap/bootstrap.min.js" type="js"}
        {include_media path="bootstrap/bootstrap-select.min.js" type="js"}
        {include_media path="bootstrap/bootstrap-checkbox.js" type="js"}
        {include_media path="bootstrap/bootstrap-radio.js" type="js"}
        {include_media path="bootstrap/bootstrap-datetimepicker.min.js" type="js"}
        {include_media path="integr.js?version=$jsAPIVersion" type="js"}
        {include_media path="ICanHaz.min.js" type="js"}
        {include_media path="dropzone.js" type="js"}
        {include_media path="sync.js" type="js"}
        {include_media path="layout/validator.js" type="js"}
        {include_media path="layout/layout.js" type="js"}
        {include_media path="layout/dict.js" type="js"}
        {include_media path="layout/menu/menu.js" type="js"}
        {include_media path="layout/menu/topmenu.js" type="js"}
        {include_media path="layout/menu/contextMenu.js" type="js"}
        {include_media path="layout/powiadomienia.js" type="js"}
        {include_media path="layout/wiadomosci.js" type="js"}
        {literal}
        <script type="text/javascript">
                // convert local timestamp to pl tz
                Date.makeSRMTimestamp = function(timestamp)
                {
                        // js odwraca strefy..
                        var clientTimezoneMs = (new Date().getTimezoneOffset() * (60*1000)) * (-1);
                        var plTimezoneMs = {/literal}{'Z'|date}{literal} * 1000;
                        var diff = plTimezoneMs - clientTimezoneMs; 
                        fixedTimestamp = timestamp + diff;
                        return new Date(fixedTimestamp);
                };
                // globalna zmienna określająca precyzję przy składaniu ofert na zapytanie ofertowe
                var globalZapytaniaPrecision = {/literal}{'zapytanie.precision'|srm_config|default:"2"}{literal};

            // globalna zmienna określająca język aplikacji w js        
            var globalJezykAplikacji = '{/literal}{$smarty.session.lang}{literal}';
            var SRM = {};
            var SRM_TRANSLATIONS = {};
            var SRM_TIMESTAMP = '{/literal}{$aktualny_timestamp}{literal}';
            var SRM_FLASH = JSON.parse('{/literal}{$flashMessage}{literal}');
            var SRM_USER_TYPE = '{/literal}{$smarty.session.nazwa}{literal}';
            var SRM_DEFAULT_CATEGORY = '{/literal}{$smarty.const.DOMYSLNA_BRANZA}{literal}';
            
            $(document).ready(function()
            {
                Layout.init();
                
                if (SRM_USER_TYPE === 'kupiec')
                {
                    Layout.Notifications.init();
                }
                
                Layout.Messages.Notification.init();
                
                var messagesParams = {};
                messagesParams.id = 0;
                messagesParams.module = null;
                messagesParams.setPulse = true;

                Layout.Messages.init(messagesParams);
            });
        </script>
    {/literal}
    {if $srm_config.parameteres.topmenu == 1}
    {literal}
        <script type="text/javascript">
            $(document).ready(function()
            {
                $.extend(Layout, 
                {
                    ListTopMenuObjects: {/literal}{$srm_config.topmenu}{literal}
                }); 
                
                Layout.Menu.TopMenu.init();
            });
        </script>
    {/literal}
    {/if}
    </head>
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <body>
        {include file='srmv2/layout/partial/elementTemplates.tpl'}
        <div id="loadingOverlay">
            <div id="loadingOverlayMask"></div>
            <div id="loadingOverlayContainer">
                <span id="loadingOverlayPercent"></span>
                <div id="loadingOverlayAnimationBG">
                    <span id="loadingOverlayAnimation"></span>
                </div>
            </div>
        </div>
        <div id="mainPage">
            {include file='srmv2/layout/header.tpl'}
            {include file='srmv2/layout/main.tpl'}
            {include file='srmv2/layout/footer.tpl'}
        </div>
        {literal}
        <!-- Start of LiveChat (www.livechatinc.com) code -->
        <script type="text/javascript">
            window.__lc = window.__lc || {};
            window.__lc.license = 7548311;
            (function() {
            var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
            lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
            })();
        </script>
        <!-- End of LiveChat code -->
        {/literal}        
    </body>
</html> 