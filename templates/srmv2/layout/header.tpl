<header id="mainPageHeaderBG">
    <div id="mainPageHeader">
        {if $smarty.session.nazwa == 'kupiec'}
        <nav id="mainMenu">
            <div id="mainMenuScroller">
                <ul id="mainMenuList">
                    <li class="new">
                        <a href="#" title="{'Nowe postępowanie'|t}">
                            <span></span>
                        </a>
                    </li>
                    <li class="apps">
                        <a href="#" title="{'Aplikacje'|t}">
                            <span></span>
                        </a>
                    </li>
                </ul>
            </div>
            <div id="mainMenuContent"></div>
        </nav>
        <div id="notificationPanel" style="display: none;">
            <div id="notificationsContainer"></div>
            <div class="showAll">
                <a href="powiadomienia,index.html" id="showAll" class="left">{'Pokaż wszystkie powiadomienia'|t}</a>
                <a href="#" id="markAsRead" class="right">{'Oznacz jako przeczytane'|t}</a>
                <div class="clear"></div>
            </div>
        </div>
        {/if}
        <div id="messagesPanel" style="display: none;">
            <div id="messagesContainer"></div>
            <div class="showAll">
                <a href="poczta,index.html" id="showAll" class="left">{'Pokaż wszystkie wiadomości'|t}</a>
                <a href="#" id="markAsRead" class="right">{'Oznacz jako przeczytane'|t}</a>
                <div class="clear"></div>
            </div>
        </div>
        <div id="userPanel" style="display:none">
            <div id="userPanelDetails">
                {if $smarty.session.avatar}
                    <div class="icon" style="background-image:url(zalaczniki/avatar/{$smarty.session.avatar});"></div>
                {else}
                    <div class="icon"></div>
                {/if}
                <div class="name">{$smarty.session.imie} {$smarty.session.nazwisko}</div>
            </div>
            <div id="userPanelMenu">
                <ul>
                    <li><a href="uzytkownik,index.html">{'Twoje dane'|t}</a></li>
                    {if $smarty.session.nazwa == 'kupiec'}
                        <li><a href="kategorieZakupowe,index.html">{'Administracja'|t}</a></li>
                    {/if}
                    <li><a href="#" id="logout">{'Wyloguj'|t}</a></li>
                </ul>
            </div>
        </div>
        <div id="pageHeaderTopbar">
            {if $smarty.session.nazwa == 'kupiec'}
                {if 'srm_version_2.start_menu'|srm_config == 1}
                    <div id="startButton">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                {/if}
                <a href="{'srm_version_2.adres_startowy'|srm_config}" class="headerIcons headerIconsHome"></a>
            {else}
            <div id="logoButton">
                <a href="{'srm_version_2.adres_startowy_dostawca'|srm_config}" /></a>
            </div> 
            {/if}
            <div id="menuItems"></div>
            <div id="pageHeaderTopbarOptions">
                <div class="personHeader">
                    {if $smarty.session.avatar}
                    <div class="icon" style="background-image:url(zalaczniki/avatar/{$smarty.session.avatar});"></div>
                    {else}
                    <div class="icon userIcon"></div>
                    {/if}
                    <div class="value">
                        {$smarty.session.imie} <br> {$smarty.session.nazwisko}
                    </div>
                    <a class="headerIcons headerIconsDropdownArrow" href="javascript:void();"></a> 
                </div>
                <div id="pageHeaderTopbarNavigation">
                    {if $smarty.session.nazwa == 'kupiec'}
                        <a class="headerIcons headerIconsInfo">
                            <div class="messageInfo" style="display: none;"></div>
                        </a>
                    {/if}
                    <a class="headerIcons headerIconsConversations">
                        <div class="messageInfo" style="display: none;"></div>
                    </a>
                </div>
                <div class="dateHeader">
                    <div class="icon headerIcons headerIconsClock"></div>
                    <div class="value">
                        <div id="TopbarClockTime" class="hour">
                           {$aktualna_godz}:{$aktualna_min}:{$aktualna_sek}
                        </div>
                        <div class="date">
                           {$aktualna_data}
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <section id="mainPageTopMenu">
        <div class="subtabs"></div>
    </section>
</header>
<div id="headerSeperator"></div>
{if $smarty.session.nazwa == 'kupiec'}
<secion id="mainPageBreadcrumbBG">
    <div id="mainPageBreadcrumb">
        <div id="mainPageBreadcrumbText"></div>
    </div>
</secion>
{/if}