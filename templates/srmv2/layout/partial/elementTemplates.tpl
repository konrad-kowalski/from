{literal}
<script id="elementLoaderTpl" type="text/html">
    <div class="loadingOverlay">
        <div class="loadingOverlayContainer">        
            <div class="loadingOverlayAnimationBG">
                <span class="loadingOverlayAnimation"></span>
            </div>
        </div>
    </div>
</script>
<script id="menuNewDocoumentTpl" type="text/html">
    <div id="menuDocuments" class="listMenu">
        <div class="headerContent">{/literal}{'Utwórz'|t}{literal}</div>
        <div class="list">
            {/literal}{if has_feature('srm_version_2.modul_rfx')}{literal}
            <div class="element">
                <a id="menuRFX" href="javascript:void(0);">
                    <div class="linkNormal">
                        <span class="icon"></span>
                        <span class="title">{/literal}{'Zapytanie'|t}{literal}</span>
                    </div>
                    <div class="linkHover">
                        <div>
                            <span class="icon"></span>
                            <span class="title">{/literal}{'Zapytanie'|t}{literal}</span>
                        </div>
                        <span class="description">{/literal}{'Przygotuj zapytanie ofertowe, określ kryteria oceny oferty handlowej, analizuj i oceniaj oferty'|t}{literal}</span>
                    </div>
                </a>
            </div>
            {/literal}{/if}{literal}
            {/literal}{if has_feature('srm_version_2.modul_aukcje')}{literal}
            <div class="element">
                <a id="menuAuction" href="javascript:void(0);">
                    <div class="linkNormal">
                        <span class="icon"></span>
                        <span class="title">{/literal}{'Aukcja'|t}{literal}</span>
                    </div>
                    <div class="linkHover">
                        <div>
                            <span class="icon"></span>
                            <span class="title">{/literal}{'Aukcja'|t}{literal}</span>
                        </div>
                        <span class="description">{/literal}{'Przygotuj aukcje elektroniczne, dobieraj algorytmy aukcyjne, śledź przebieg aukcji i generuj raporty'|t}{literal}</span>
                    </div>
                </a>
            </div>
            {/literal}{/if}{literal}
            {/literal}{if has_feature('srm_version_2.modul_rfx')}{literal}
            <div class="element">
                <a id="menuSpecialShopping" href="rfx,nowy,zakupawaryjny.html">
                    <div class="linkNormal">
                        <span class="icon"></span>
                        <span class="title">{/literal}{'Zakup specjalny'|t}{literal}</span>
                    </div>
                    <div class="linkHover">
                        <div>
                            <span class="icon"></span>
                            <span class="title">{/literal}{'Zakup specjalny'|t}{literal}</span>
                        </div>
                        <span class="description">{/literal}{'Przygotuj zakup w innej formie ewidencjonuj dokonane zakupy, zapisz zakup awaryjny'|t}{literal}</span>
                    </div>
                </a>
            </div>
            {/literal}{/if}{literal}
        </div>
    </div>
</script>
<script id="menuRFXTpl" type="text/html">
    <div id="menuDocuments" class="listMenu">
        <div class="headerContent">
            <span class="back"></span>
            {/literal}{'Zapytanie'|t}{literal}
        </div>
        <div class="list">
            <div class="element">
                <a id="menuRFXInstant" href="javascript:Layout.Menu.newQuickRFX();">
                    <div class="linkNormal">
                        <span class="icon"></span>
                        <span class="title">{/literal}{'Szybkie'|t}{literal}</span>
                    </div>
                    <div class="linkHover">
                        <div>
                            <span class="icon"></span>
                            <span class="title">{/literal}{'Szybkie'|t}{literal}</span>
                        </div>
                        <span class="description">{/literal}{'Wprowadź nazwę produktu i wyślij zapytanie ofertowe'|t}{literal}</span>
                    </div>
                </a>
            </div>
            <div class="element">
                <a id="menuRFXStandard" href="rfx,nowy,warunki.html">
                    <div class="linkNormal">
                        <span class="icon"></span>
                        <span class="title">{/literal}{'Standardowe'|t}{literal}</span>
                    </div>
                    <div class="linkHover">
                        <div>
                            <span class="icon"></span>
                            <span class="title">{/literal}{'Standardowe'|t}{literal}</span>
                        </div>
                        <span class="description">{/literal}{'Wykorzystaj prosty formularz składania zapytań ofertowych'|t}{literal}</span>
                    </div>
                </a>
            </div>
            <div class="element">
                <a id="menuRFXFull" href="rfx,nowy,warunki.html?edit=full">
                    <div class="linkNormal">
                        <span class="icon"></span>
                        <span class="title">{/literal}{'Zaawansowane'|t}{literal}</span>
                    </div>
                    <div class="linkHover">
                        <div>
                            <span class="icon"></span>
                            <span class="title">{/literal}{'Zaawansowane'|t}{literal}</span>
                        </div>
                        <span class="description">{/literal}{'Stwórz zapytanie ofertowe, wprowadź kryteria wyboru oferty, zadaj pytania i korzystaj z zaawansowanych opcji'|t}{literal}</span>
                    </div>
                </a>
            </div>
            <div class="element">
                <a id="menuTemplates" href="rfxSzablony,index.html">
                    <div class="linkNormal">
                        <span class="icon"></span>
                        <span class="title">{/literal}{'Szablony'|t}{literal}</span>
                    </div>
                    <div class="linkHover">
                        <div>
                            <span class="icon"></span>
                            <span class="title">{/literal}{'Szablony'|t}{literal}</span>
                        </div>
                        <span class="description">{/literal}{'Wygeneruj nowe zapytanie na bazie wcześniej zapisanych szablonów'|t}{literal}</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</script>
<script id="menuAuctionTpl" type="text/html">
    <div id="menuDocuments" class="listMenu">
        <div class="headerContent">
            <span class="back"></span> 
            {/literal}{'Aukcje'|t}{literal}
        </div>
        <div class="list">
            <div class="element">
                <a id="menuAuctionDescending" href="aukcje,nowa,znizkowa.html">
                    <div class="linkNormal">
                        <span class="icon"></span>
                        <span class="title">{/literal}{'Zniżkowa'|t}{literal}</span>
                    </div>
                    <div class="linkHover">
                        <div>
                            <span class="icon"></span>
                            <span class="title">{/literal}{'Zniżkowa'|t}{literal}</span>
                        </div>
                        <span class="description">{/literal}{'Organizuj aukcję na kryterium cenowe na jeden lub wiele produktów'|t}{literal}</span>
                    </div>
                </a>
            </div>
            <div class="element">
                <a id="menuAuctionAscending" href="aukcje,nowa,zwyzkowa.html">
                    <div class="linkNormal">
                        <span class="icon"></span>
                        <span class="title">{/literal}{'Zwyżkowa'|t}{literal}</span>
                    </div>
                    <div class="linkHover">
                        <div>
                            <span class="icon"></span>
                            <span class="title">{/literal}{'Zwyżkowa'|t}{literal}</span>
                        </div>
                        <span class="description">{/literal}{'Organizuj aukcję sprzedażową lub zwyżkową na rabat na jeden lub wiele produktów'|t}{literal}</span>
                    </div>
                </a>
            </div>
            <div class="element">
                <a id="menuAuctionMultiobject" href="aukcje,nowa,multiobiektowa.html">
                    <div class="linkNormal">
                        <span class="icon"></span>
                        <span class="title">{/literal}{'Multiobiektowa'|t}{literal}</span>
                    </div>
                    <div class="linkHover">
                        <div>
                            <span class="icon"></span>
                            <span class="title">{/literal}{'Multiobiektowa'|t}{literal}</span>
                        </div>
                        <span class="description">{/literal}{'Organizuj aukcję pozwalającą na rozbicie przedmiotu postępowania na wielu dostawców'|t}{literal}</span>
                    </div>
                </a>
            </div>
            <div class="element">
                <a id="menuAuctionJapan" href="aukcje,nowa,japonska.html">
                    <div class="linkNormal">
                        <span class="icon"></span>
                        <span class="title">{/literal}{'Japońska'|t}{literal}</span>
                    </div>
                    <div class="linkHover">
                        <div>
                            <span class="icon"></span>
                            <span class="title">{/literal}{'Japońska'|t}{literal}</span>
                        </div>
                        <span class="description">{/literal}{'Organizuj aukcję na cenę w przypadku niewielu dostawców lub znacznych odchyleń w ofertach'|t}{literal}</span>
                    </div>
                </a>
            </div>
            <div class="element">
                <a id="menuTemplates" href="aukcjeSzablony,index.html">
                    <div class="linkNormal">
                        <span class="icon"></span>
                        <span class="title">{/literal}{'Szablony'|t}{literal}</span>
                    </div>
                    <div class="linkHover">
                        <div>
                            <span class="icon"></span>
                            <span class="title">{/literal}{'Szablony'|t}{literal}</span>
                        </div>
                        <span class="description">{/literal}{'Wygeneruj nową aukcję na bazie wcześniej zapisanych szablonów'|t}{literal}</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</script>
<script id="menuAppsTpl" type="text/html">
    <div id="menuApps" class="listMenu">
        <div class="headerContent">{/literal}{'Aplikacje'|t}{literal}</div>
        <div class="list">
            {/literal}{if has_feature('srm_version_2.modul_pulpit')}{literal}
            <div class="element">
                <a id="menuPulpit" href="pulpit,index.html">
                    <div class="linkNormal">
                        <span class="icon"></span>
                        <span class="title">{/literal}{'Pulpit'|t}{literal}</span>
                    </div>
                    <div class="linkHover">
                        <div>
                            <span class="icon"></span>
                            <span class="title">{/literal}{'Pulpit'|t}{literal}</span>
                        </div>
                        <span class="description">{/literal}{'Sprawdź statystyki dla prowadzonych procesów zakupowych'|t}{literal}</span>
                    </div>
                </a>
            </div>
            {/literal}{/if}{literal}
            <div class="element">
                <a id="menuOrders" href="{/literal}{'srm_version_2.adres_startowy'|srm_config}{literal}">
                    <div class="linkNormal">
                        <span class="icon"></span>
                        <span class="title">{/literal}{'Zakupy'|t}{literal}</span>
                    </div>
                    <div class="linkHover">
                        <div>
                            <span class="icon"></span>
                            <span class="title">{/literal}{'Zakupy'|t}{literal}</span>
                        </div>
                        <span class="description">{/literal}{'Realizuj proces zakupowy z wykorzystaniem elektronicznych narzędzi'|t}{literal}</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</script>
<script id="topMenuItem" type="text/html">
    <a href="{{url}}" class="{{#active}}active{{/active}} {{#positionRight}}right{{/positionRight}}">{{name}}</a>
</script>
<script id="topMenuSubItem" type="text/html">
    <a href="{{url}}" class="tab{{#active}} active{{/active}}">{{name}}</a>
</script>
<script id="iconTpl" type="text/html">
    <span class="{{iconGroupClass}}} {{iconClass}}" {{#tooltip}}data-toggle="tooltip"{{/tooltip}} title="{{title}}"></span>
</script>
<script id="selectOptionTpl" type="text/html"> 
    <option value="{{value}}" class="{{classCSS}}}" {{#selected}}selected="selected"{{/selected}} {{#disabled}}disabled="disabled"{{/disabled}}>{{name}}</option>
</script>
<script id="tooltipTpl" type="text/html">
    <div class="tooltip {{class}}}" role="tooltip">
        <div class="tooltip-arrow"></div>
        <div class="tooltip-inner"></div>
    </div>
</script>        
<script id="confirmModalTpl" type="text/html">
    <div id="{{id}}}" class="modal fade" role="dialog" {{#backdrop}}data-backdrop="{{backdrop}}"{{/backdrop}}>
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center {{size}}}">
                <div class="modal-content">
                    <div class="modal-header">
                        {{^noClose}}
                            <button type="button" class="close" data-dismiss="modal">&nbsp;</button>
                        {{/noClose}}
                        <h4 class="modal-title">{{title}}</h4>
                    </div>
                    <div class="modal-body {{bodyClass}}">{{content}}</div>
                    <div class="modal-footer">
                        <div class="footer-buttons">
                            <button type="button" id="confirmModalDismiss" class="btn actionButton {{dismissClass}}" data-dismiss="modal">{{dismissText}}</button>
                            <button type="button" id="confirmModalAccept" class="btn actionButton {{acceptClass}}">{{acceptText}}</button>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>        
<script id="dialogModalTpl" type="text/html">
    <div id="{{id}}}" class="modal fade" role="dialog" {{#backdrop}}data-backdrop="{{backdrop}}"{{/backdrop}}>
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center {{size}}}">
                <div class="modal-content">
                    <div class="modal-header">
                        {{^noClose}}
                            <button type="button" class="close" data-dismiss="modal">&nbsp;</button>
                        {{/noClose}}
                        <h4 class="modal-title">{{title}}</h4>
                    </div>
                    <div class="modal-body {{bodyClass}}">{{content}}</div>
                </div>
            </div>
        </div>
    </div>
</script>
<script id="messageTpl" type="text/html">
    <section class="messageBox messageBoxStatic {{type}} {{cssClass}}" style="display:block;">
        <div class="left">
            <span class="icon">
                <span class="messageIcons messageIconsWarning"></span>
            </span>
            <span class="text">{{message}}</span>
        </div>
        {{#button}}
        <div class="close">
            <span></span>
        </div>
        {{/button}}
    </section>
</script>
<script id="dialogMessageTpl" type="text/html">
    <div class="dialogMessageBox {{type}} confimrModal">
        <div  class="dialogMessageBoxContent">
            <div class="icon">
                <span></span>
            </div>
            <div class="text">{{&message}}</div>
            <div class="clear"></div>
        </div>
    </div>
</script>
<script id="closeModalFooterTpl" type="text/html">
    <div class="modal-footer">
        <div class="footer-buttons">
            <button type="button" id="closeModalClose" class="btn actionButton red">{/literal}{'Zamknij'|t}{literal}</button>
        </div>
        <div class="clear"></div>
    </div>
</script>
<script id="closeTestAuctionModalFooterTpl" type="text/html">
    <div class="modal-footer">
        <div class="footer-buttons">
            <button type="button" id="closeTestAuctionModalRecreate" class="btn actionButton orange">{/literal}{'Włącz ponownie'|t}{literal}</button>
            <button type="button" id="closeModalClose" class="btn actionButton red" data-dismiss="modal">{/literal}{'Zamknij'|t}{literal}</button>
        </div>
        <div class="clear"></div>
    </div>
</script>
<script id="closeMainAuctionModalFooterTpl" type="text/html">
    <div class="modal-footer">
        <div class="footer-buttons">
            <button type="button" id="closeMainAuctionModalClose" class="btn actionButton">{/literal}{'Przejdź do aukcji głównej'|t}{literal}</button>
        </div>
        <div class="clear"></div>
    </div>
</script>
<script id="autocompleteTpl" type="text/html">
    <div id="{{id}}" class="autocomplete {{classCSS}}">
        <div class="smallLoadingOverlayAnimationBG">
            <div class="smallLoadingOverlayAnimation"></div>
        </div>
        <div  class="autocomplete-list">
            <div class="autocomplete-container">
                <ul class="autocomplete-menu"></ul>
            </div>
        </div>
    </div>
</script>
<script id="autocompleteItemTpl" type="text/html">
    <li data-id="{{id}}">
        <div class="autocomplete-name">{{name}}</div>
        {{#info}}
        <div class="autocomplete-info">{{info}}</div>
        {{/info}}
    </li>
</script>
<script id="categoryTreeSelectTpl" type="text/html">
    <div id="{{id}}_container" class="categoryTreeContainer category-select {{class}}">
        {{#level}}
        <input id="{{id}}_level" name="categoryLevel_{{id}}" value="" type="hidden" />
        {{/level}}
        <input id="{{id}}" name="{{id}}" value="" type="hidden" />
        <button id="{{id}}_button" class="categoryTreeButton" type="button" {{#name}}name="{{name}}"{{/name}}>
            <span id="{{id}}_buttonText" class="categoryTreeButtonText"></span>
            <span class="caret"></span>
        </button>
    </div>
</script>
<script id="categoryTreeMultiSelectTpl" type="text/html">
    <div id="{{id}}_container" class="categoryTreeContainer category-select {{class}}">
        <select id="{{id}}" name="{{id}}[]" class="hidden" multiple="multiple"></select>
        <button id="{{id}}_button" class="categoryTreeButton" type="button">
            <span id="{{id}}_buttonText" class="categoryTreeButtonText"></span>
            <span class="caret"></span>
        </button>
    </div>
</script>
<script id="categoryTreeMultiSelectOptionTpl" type="text/html">
    <option value="{{id}}|{{level}}" selected="selected">{{id}}</option>
</script>
<script id="categoryTreeTpl" type="text/html">
    <div id="{{id}}_listContainer" class="categoryTreeMenu">
        <div id="{{id}}_listContainerMask" class="containerMask"></div>
        <ul id="{{id}}_list" class="categoryTreeList"></ul>
    </div>
</script>
<script id="categoryTreeListButtonTpl" type="text/html">
    <div id="{{global_id}}_{{id}}_chlidrenButton" class="moreCategoriesButton" category-id="{{id}}" category-level="{{level}}" children-loaded="0">
        <span id="{{global_id}}_{{id}}_chlidrenButtonIcon" class="moreCategoriesButtonIcon"></span>
    </div>
</script>
<script id="categoryTreeListClearTpl" type="text/html">
    <div class="clear"></div>
</script>
<script id="categoryTreeChildrenListTpl" type="text/html">
    <ul id="{{global_id}}_{{id}}_childrenList" class="categoryTreeList childList {{class}}}">
    </ul>
</script>
<script id="categoryTreeListTpl" type="text/html">
    <li id="{{global_id}}_{{value}}_listElement" data-original-index="{{index}}" class="categoryListElement">
        <div class="listRow">
            <a id="{{global_id}}_{{value}}_listOption" class="categoryLink" tabindex="{{index}}">{{name}}</a>
        </div>
    </li>
</script>
<script id="categoryTreeLoaderTpl" type="text/html">
    <div class="smallLoadingOverlayAnimationBG">
        <span class="smallLoadingOverlayAnimation"></span>
    </div>
</script>
<script id="paginationTpl" type="text/html">
    <div class="mainPageContentPagination paginationLinksContainer">
        <div class="paginationLinks">
            <a class="navigation first" href="javascript:void(0);">
                <span class="button">«</span>
            </a>
            <a class="navigation prev" href="javascript:void(0);">
                <span class="button">‹</span>
            </a>
            <div class="paginationPageLinks"></div>
            <a class="navigation next" href="javascript:void(0);">
                <span class="button">›</span>
            </a>
            <a class="navigation last" href="javascript:void(0);">
                <span class="button">»</span>
            </a>
        </div>
    </div>
</script>
<script id="paginationPageTpl" type="text/html">
    <a {{#active}}class="active"{{/active}} href="javascript:void(0);">{{page}}</a>
</script>
<script id="paginationPageSeparatorTpl" type="text/html">
    <a class="separator" href="javascript:void(0);">&hellip;</a>
</script>
<script id="noDataRowTpl" type="text/html"> 
    <tr class="noDataRow">
        <td colspan="{{colspan}}">{/literal}{'Brak pozycji'|t}{literal}</td>
    </tr>
</script>
<script id="progressTabTpl" type="text/html">
    <a class="tab {{classCSS}}" id="tab{{id}}}" href="javascript:void(0)">
        <div>
            <span class="left"></span>
            <span class="content">{{name}}</span>
            <span class="right"></span>
        </div>
    </a>
</script>
<script id="actionButtonTpl" type="text/html">
    <input type="button" value="{{value}}" class="actionButton {{cssClass}}}" id="{{id}}}" />
</script>
<script id="contextMenuTpl" type="text/html">
    <div id="contextMenuContent" class="mainPageContentWindow"></div>
</script>
<script id="contextMenuListTpl" type="text/html">
    <ul class="optionList"></ul>
</script>
<script id="contextMenuRowTpl" type="text/html">
    <li>
        <a href="{{adres}}" class="{{#potwierdzenie}}confirmModal{{/potwierdzenie}}" {{#potwierdzenie}} confirm-type="{{potwierdzenie_typ}}" confirm-title="{{potwierdzenie_tytul}}" confirm-text="{{potwierdzenie_tresc}}"{{/potwierdzenie}}>
           <span class="icon" style="background-image: url('{{ikona}}'); background-position: {{ikona_x}}px {{ikona_y}}px"></span>
           <span class="text">{{nazwa}}</span>
        </a>
    </li>
</script>
<script id="contextMenuRowJsTpl" type="text/html">
    <li>
        <a href="javascript:{{js}}" class="{{#potwierdzenie}}confirmModal{{/potwierdzenie}}" {{#potwierdzenie}} confirm-type="{{potwierdzenie_typ}}" confirm-title="{{potwierdzenie_tytul}}" confirm-text="{{potwierdzenie_tresc}}"{{/potwierdzenie}}{{#potwierdzenie}} confirm-type="{{potwierdzenie_typ}}" confirm-title="{{potwierdzenie_tytul}}" confirm-text="{{potwierdzenie_tresc}}"{{/potwierdzenie}}>
           <span class="icon" style="background-image: url('{{ikona}}'); background-position: {{ikona_x}}px {{ikona_y}}px"></span>
           <span class="text">{{nazwa}}</span>
        </a>
    </li>
</script>
<script id="contextMenuRowCategoryTpl" type="text/html">
    <li>
        <a href="javascript:void(0);" class="group">
            <span class="icon" style="background-image: url('{{ikona}}'); background-position: {{ikona_x}}px {{ikona_y}}px"></span>
            <span class="text">{{nazwa}}</span>
            <span class="button">
                <span class="caret"></span>
            </span>
        </a>
    </li>
</script>
<script id="goToListTpl" type="text/html">
    <section id="gotToSectuion">
        <div class="list"></div>
        <div class="clear"></div>
    </section>
</script>
<script id="goToElementTpl" type="text/html">
    <div class="element">
        <a id="goTo{{}css_class}}" class="{{^active}}disabled{{/active}}" href="{{url}}">
            <div class="linkNormal">
                <span class="icon typeIcon manual"></span>
                <span class="title">{{title}}</span>
            </div>
            <div class="linkHover">
                <div>
                    <span class="icon typeIcon manual small"></span>
                    <span class="title">{{title}}</span>
                </div>
                <span class="description">{{description}}</span>
            </div>
        </a>
    </div>
</script>
<script id="sectionImportFromFileModalTpl" type="text/html">
    <div id="{{id}}Modal" class="modal fade" role="dialog">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center medium">>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&nbsp;</button>
                        <h4 class="modal-title">{{title}}</h4>
                    </div>
                    <div class="modal-body dataFields">
                    </div>
                    {{#footer}}
                    <div class="modal-footer">
                        <div class="footer-buttons">
                            <a href="{{template_file_url}}" title="{/literal}{'Pobierz szablon'|t}{literal}">{/literal}{'Pobierz szablon'|t}{literal}</a>
                            <button type="button" class="btn actionButton" data-dismiss="modal">{/literal}{'Anuluj'|t}{literal}</button>
                            <button type="button" class="btn actionButton" id="{{id}}ModalAcceptButton">{/literal}{'Importuj'|t}{literal}</button>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    {{/footer}}
                </div>
            </div>
        </div>
    </div>
</script>
<script id="importFromFileModalTpl" type="text/html">
    <section class="dataTable">
        <div class="dataField">
            <form id="importModalUploadzone" class="dropzone"></form>
        </div>
    </section>
    <div class="clear"></div>
</script>
<script id="importFromFileCategoryModalTpl" type="text/html">
    <section class="dataTable">
        <div class="dataField">
            <form id="importModalUploadzone" class="dropzone"></form>
        </div>
        <div class="formField">
            <div class="fieldLabel">
                {/literal}{'Kategoria zakupowa'|t}:{literal}
            </div>
            <div class="field">
                <span id="importCategoryContainer"></span>
            </div>
        </div>
    </section>  
    <div class="clear"></div>
</script>
<script id="sectionImportFromFileErrorsModalTpl" type="text/html">
    <div id="{{id}}Modal" class="modal fade" role="dialog">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center medium">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&nbsp;</button>
                        <h4 class="modal-title">{{title}}</h4>
                    </div>
                    <div class="modal-body">
                        <p>{/literal}{'W importowanym pliku wystąpiły poniższe błędy'|t}{literal}:</p>
                        <section class="elemntsListContainer errorsList small">
                            <div id="{{id}}List" class="elemntsList"></div>
                        </section>  
                        <div class="clear"></div>
                    </div>
                    {{#footer}}
                    <div class="modal-footer">
                        <button type="button" class="btn actionButton orange" data-dismiss="modal">{/literal}{'Ok'|t}{literal}</button>
                    </div>
                    {{/footer}}
                </div>
            </div>
        </div>
    </div>
</script>
<script id="importFromFileErrorMessageModalTpl" type="text/html"> 
    <p>{{error_message}}</p>
</script>
<script id="quoteEditorTpl" type="text/html">
    {{#content}}
        <span>
            <blockquote>{{content}}</blockquote>
            <p></p>
        </span>
    {{/content}}
</script>
<script id="messagesModalTpl" type="text/html"> 
    <section class="dataFields">
        <div id="messageModalFields" class="dataFieldsForm">
            <div class="dataField">
                <div class="label">{/literal}{'Treść wiadomości'|t}{literal}</div>
                <div>
                    <textarea id="messageContent" name="content">{{content}}</textarea>
                </div>
            </div>
        </div>
    </section>
    <section class="dataFields">
        <div class="dataFieldsForm">
            <div class="dataField sectionAttachments">
                <div class="label">{/literal}{'Załączniki'|t}{literal}</div>
                <section id="sectionContentAtatachments" class="dataTable attachmentsList">
                    <div class="dataTableContent">
                        <table id="contentAtatachmentsList">
                            <tbody>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </section>
                <form id="sectionContentUploadzone" class="dropzone"></form>
            </div>
        </div>
    </section>
</script>
<script id="messagesNewModalTpl" type="text/html"> 
    <div class="dataField fieldsBlock">
        <div class="label">{/literal}{'Tytuł wiadomości'|t}{literal}</div>
        <div class="field">
            <input id="messageTitle" name="title" class="inline" value="{{title}}" type="text">
        </div>
    </div>
    <div class="dataField fieldsBlock">
        <div class="label">{/literal}{'Nazwa odbiorcy'|t}{literal}</div>
        <div id="messageRecipient_main" class="fbautocomplete-main-div ui-helper-clearfix">
            <div class="lineWrapper">
                <input id="messageRecipient" name="recipients" type="text" value="" />
                <a class="dropDown" href="javascript:void(0)">&nbsp;</a>
            </div>
        </div>
        <div id="recipientsBoxes">
            <div id="additionalRecipients">
                <div class="additionalLabel">
                    {/literal}{'Oraz'|t}{literal}:
                </div>
                <div id="additionalRecipientsSelectpicker">
                    <div>
                        <a href="javascript:void(0);" id="additionalRecipientsButton">
                            <span id="additionalRecipientsCount">0</span> {/literal}{'dostawców'|t}{literal}<span class="caret"></span>
                        </a>
                        <div id="additionalRecipientsList">
                            <ul></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>
<script id="messagesProviderNewModalTpl" type="text/html">
    <div class="dataField fieldsBlock">
        <div class="label">{/literal}{'Tytuł wiadomości'|t}{literal}</div>
        <div class="field">
            <input id="messageTitle" name="title" class="inline" value="{{title}}" type="text">
        </div>
    </div>
</script>
<script id="messagesSingleResponseModalTpl" type="text/html"> 
    <div class="dataField fieldsBlock">
        <div class="label">{/literal}{'Tytuł wiadomości'|t}{literal}</div>
        <div class="field">{{title}}</div>
    </div>
    <div class="dataField fieldsBlock">
        <div class="label">{/literal}{'Nazwa odbiorcy'|t}{literal}</div>
        <div class="field">{{recipient.company}}</div>
    </div>
</script>
<script id="messagesResponseModalTpl" type="text/html"> 
    <div class="dataField fieldsBlock">
        <div class="label">{/literal}{'Tytuł wiadomości'|t}{literal}</div>
        <div class="field">{{title}}</div>
    </div>
    <div class="dataField fieldsBlock">
        <div class="label">{/literal}{'Nazwa odbiorcy'|t}{literal}</div>
        <div id="messageRecipient_main" class="fbautocomplete-main-div ui-helper-clearfix">
            <div class="lineWrapper">
                <input id="messageRecipient" name="recipients" type="text" value="" />
                <a class="dropDown" href="javascript:void(0)" onclick="$('#messageRecipient').autocomplete('search', ' '); $('#messageRecipient').focus();">&nbsp;</a>
            </div>
        </div>
        <div id="recipientsBoxes">
            <div id="additionalRecipients">
                <div class="additionalLabel">
                    {/literal}{'Oraz'|t}{literal}:
                </div>
                <div id="additionalRecipientsSelectpicker">
                    <div>
                        <a href="javascript:void(0);" id="additionalRecipientsButton">
                            <span id="additionalRecipientsCount">0</span> {/literal}{'dostawców'|t}{literal}<span class="caret"></span>
                        </a>
                        <div id="additionalRecipientsList">
                            <ul></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>
<script id="messagesRecipientDropdownTpl" type="text/html">
    <li>
        <div class="recipientIcon">
            <span style="background-image:url('{{avatar}}')"></span>
        </div>
        <div class="recipientDetails">
            <div class="name">{{name}}</div>
            <div class="email">{{email}}</div>
        </div>
        <div class="closeButton">
            <a href="javascript:void(0);"><span class="closeIcon"></span></a>
        </div>
        {{/noClose}}
    </li>
</script>
<script id="messagesNewAdditionalRecipientTpl" type="text/html">
    <li>
        <div class="recipientIcon">
            <span style="background-image:url('{{avatar}}')"></span>
        </div>
        <div class="recipientDetails">
            <div class="name">{{fullname}}</div>
            <div class="email">{{email}}</div>
        </div>
        <div class="closeButton">
            <a href="javascript:void(0);"><span class="closeIcon"></span></a>
        </div>
    </li>
</script>
<script id="messagesNewRecipientModalTpl" type="text/html">
    <div class="box" title="{{title}}" data-toggle="tooltip">
        <span class="closeIcon"></span>
        <span class="name">{{name}}</span>
    </div>
</script>
<script id="messagesModalFooterTpl" type="text/html">
    <div class="modal-footer">
        <div class="footer-buttons">
            <button type="button" id="messagesModalCancel" class="btn actionButton" data-dismiss="modal">{/literal}{'Anuluj'|t}{literal}</button>
            <button type="button" id="messagesModalSave" class="btn actionButton">{/literal}{'Zapisz'|t}{literal}</button>
            <button type="button" id="messagesModalSend" class="btn actionButton orange">{/literal}{'Wyślij'|t}{literal}</button>
        </div>
        <div class="clear"></div>
    </div>
</script>
<script id="attachmentsListTpl" type="text/html">
    <tr class="file">
        <td>
            <span class="fileIcon {{type}}"></span>
        </td>
        <td>
            <a href="{{url_download}}"  title="{{name}}">{{name}}</a>
        </td>
        <td>
            <a href="{{url_remove}}" class="deleteButton" data-toggle="tooltip" title="{/literal}{'Usuń'|t}{literal}"></a>
        </td>
    </tr>
</script>
<script id="deleteOfferAttachmentsTpl" type="text/html"> 
    <input type="hidden" name="delete_offer_attachments[]" value="{{value}}"/>
</script>
<script id="categoryRowTpl" type="text/html"> 
    <tr class="level{{level}}">
        <td class="firstColumn">
            <div class="spacing"></div>
            <div class="numbers">{{number}}</div>
        </td>
        <td class="secondColumn">
            <div class="icons">
                {{#add}}
                    <a href="#" class="addCategory editCategoryIcons addIcon" data-toggle="tooltip" title="{/literal}{'Dodaj'|t}{literal}" data-id="{{category_id}}" data-level="{{level}}"></a>
                {{/add}}
                {{^blocked}}
                    <a href="#" class="removeCategory editCategoryIcons deleteIcon tooltip66"  data-toggle="tooltip" title="{/literal}{'Usuń'|t}{literal}" data-id="{{category_id}}" data-level="{{level}}" data-index="{{id}}"></a>
                {{/blocked}}
            </div>
        </td>
    </tr>
</script>
<script id="categoryNamesTpl" type="text/html">
    {{#names}}
        <td>
            <input name="category[{{category_id}}][{{name}}]" value="{{value}}" data-id="{{id}}" data-locale="{{locale}}" {{#blocked}}disabled{{/blocked}}> 
        </td>
    {{/names}}
</script>
<script id="categoryTableHeaderTpl" type="text/html">
    {{#names}}
        <th class="locale">{/literal}{'Nazwa'|t}{literal} {{name}}</th>
    {{/names}}
</script>
<script id="categoryTableSecondHeaderTpl" type="text/html">
    {{#names}}
        <td class="locale">{/literal}{'Wszystkie kategorie'|t}{literal}</td>
    {{/names}}
</script>
<script id="removeCategoryTpl" type="text/html"> 
    <div class="dialogMessageBox {{type}} confimrModal">
        <div  class="dialogMessageBoxContent">
            <div class="icon">
                <span></span>
            </div>
            <div class="text">
                {/literal}{'Kategoria'|t}{literal}:
                <br>
                {{#secondParentName}}
                    {{secondParentName}} <span class="nextIcon"></span>
                {{/secondParentName}}
                {{#firstParentName}}
                    {{firstParentName}} <span class="nextIcon"></span>
                {{/firstParentName}}
                <span class="bold">{{name}}</span>
            </div>
            <p>{/literal}{'Wskaż kategorię, do której mają zostać przeniesione produkty i dostawcy'|t}{literal}</p>
            <section class="dataTable">
                <div class="dataTableTopSection">
                    <div class="formField filter">
                        <div class="fieldLabel">
                        {/literal}{'Kategoria zakupowa'|t}{literal}:
                        </div>
                        <div class="field">
                            <div id="DBCategoryContainer"></div>
                        </div>
                    </div>
                </div>
            </section> 
            <div class="clear"></div>
        </div>
    </div>
</script>
<script id="addCategoryTpl" type="text/html"> 
    <div id="addCategoryModal" class="modal fade" role="dialog">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center medium">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&nbsp;</button>
                        <h4 class="modal-title">{/literal}{'Nowe podkategorie'|t}{literal}</h4>
                    </div>
                    <div class="popupContent">
                        <section>
                            <input type="hidden" name="parent" value="{{parent}}">
                            <input type="hidden" name="level" value="{{level}}">
                            <div class="formFields" id="newCategoriesList"></div>
                            <div class="formRow rightButton">
                                <button type="button" class="btn actionButton orange" id="addNextCategory">{/literal}{'Dodaj kolejną'|t}{literal}</button>
                            </div>
                            <div class="clear">
                        </section>               
                    </div>
                    <div class="modal-footer">
                        <div class="footer-buttons">
                            <button type="button" class="btn actionButton" data-dismiss="modal">{/literal}{'Anuluj'|t}{literal}</button>
                            <button type="button" class="btn actionButton orange" id="modalAcceptButton">{/literal}{'Dodaj'|t}{literal}</button>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>
<script id="newCategoryRowTpl" type="text/html">
    <div class="formRow inline">
        <input name="newCategory[{{index}}][name]" value="" data-id="{{index}}" type="text">
        <a href="javascript:void(0);" class="removeCategory deleteButton inline" data-toggle="tooltip" title="{/literal}{'Usuń'|t}{literal}"></a>
    </div>
</script>
<script id="notificationItemTpl" type="text/html">
    <div class="item{{^read}} green{{/read}}">
        <a href="powiadomienia,oznaczPrzeczytane,{{id}}.html">
            <div class="icon">
                <div class="{{avatar_type}}" {{#avatar_link}}style="background-image: url({{avatar_link}});"{{/avatar_link}}></div>
            </div>
            <div class="content">
                {{#message_provider_name}}<div class="text"><span class="bold">{{message_provider_name}}</span></div>{{/message_provider_name}}
                <div class="text"><span class="bold">{{message_header_name}}</span> {{message_text}} <span class="bold">{{message_index}}</span></div>
                <div class="date">{{date}} {{time}}</div>
            </div>
        </a>
    </div>
</script>
<script id="messageItemTpl" type="text/html">
    <div class="item{{^read}} green{{/read}}">
        <a href="{{url}}">
            <div class="icon">
                {{#avatar}}
                    <div style="background-image: url(zalaczniki/avatar/{{avatar}});"></div>
                {{/avatar}}
                {{^avatar}}
                    <div class="user"></div>
                {{/avatar}}
            </div>
            <div class="content">
                <div class="text"><span class="bold">{{title}}</span></div>
                <div class="date">{{date}} {{time}}</div>
            </div>
        </a>
    </div>
</script>
<script id="noNotificationsTpl" type="text/html">
    <div class="item">
        <div class="noDataRow">
            {/literal}{'Brak powiadomień'|t}{literal}
        </div>
    </div>
</script>
<script id="optionsSelectTpl" type="text/html">
    {{#options}}
        <option value="{{value}}" class="{{classCSS}}}" {{#selected}}selected="selected"{{/selected}} {{#disabled}}disabled="disabled"{{/disabled}}>{{name}}</option>
    {{/options}}
</script>
{/literal}