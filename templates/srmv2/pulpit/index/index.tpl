{include file="srmv2/pulpit/partial/elementTemplates.tpl"}
{include_media path="layout/analizaBiznesowa.css" type="css"}
{include_media path="layout/pulpit.css" type="css"}
{include_media path="layout/pulpit.js" type="js"}
{include_media path="lib/highcharts/highcharts.js" type="js"}
{include_media path="lib/highcharts/modules/no-data-to-display.src.js" type="js"}
{include_media path="lib/highcharts/modules/highcharts-3d.js" type="js"}
{literal}
<script type="text/javascript">
    $(document).ready(function ()
    {
        Layout.Desktop.init();
    });
</script>
{/literal}
<section id="menuDesktop">
    <div class="firstRow">
        <div class="panel" id="publishedRFX">
            <div class="header">
                <div class="icon"></div>
                <div class="text">{'Wysłanych zapytań'|t}</div>
            </div>
            <div class="content">
                <div class="number">
                    <div class="value"></div>
                </div>
            </div>
        </div>
        <div class="panel" id="offersRFX">
            <div class="header">
                <div class="icon"></div>
                <div class="text">{'Średnia ofert na zapytanie'|t}</div>
            </div>
            <div class="content">
                <div class="number">
                    <div class="value"></div>
                </div>
            </div>
        </div>
        <div class="panel" id="timeRFX">
            <div class="header">
                <div class="icon"></div>
                <div class="text">{'Średni czas trwania zapytania'|t}</div>
            </div>
            <div class="content">
                <div class="number">
                    <div class="numbers">
                        <div class="value"></div>
                    </div>
                    <div class="chart">
                        <div id="barTimeRFXChart"></div>
                        <div class="barChartBottomLine"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel" id="activeProviders">
            <div class="header">
                <div class="icon"></div>
                <div class="text">{'Aktywni dostawcy'|t}</div>
            </div>
            <div class="content">
                <div class="number">
                    <div class="value"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="secondRow">
        <div class="panel" id="publishedAuctions">
            <div class="header">
                <div class="icon"></div>
                <div class="text">{'Przeprowadzonych aukcji'|t}</div>
            </div>
            <div class="content">
                <div class="number">
                    <div class="numbers">
                        <div class="value">17</div>
                    </div>
                    <div class="chart">
                        <div id="pieChartAuctions"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel" id="topAuctions">
            <div class="header">
                <div class="icon"></div>
                <div class="text">{'TOP strategii aukcyjnych'|t}</div>
            </div>
            <div class="content number">
                <div class="value">
                    <div id="barTopChart"></div>
                </div>
            </div>
        </div>
        <div class="panel" id="savingsAuctions">
            <div class="header">
                <div class="icon"></div>
                <div class="text">{'Oszczędności na aukcjach'|t}</div>
            </div>
            <div class="content">
                <div class="number">
                    <div class="numbers">
                        <div class="value twoLine">
                            <div class="total"></div>
                            <div class="currency">PLN</div>
                        </div>
                    </div>
                    <div class="chart">
                        <div id="pieChartSavings"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>